<?php

namespace Database\Seeders;

use App\Models\Categorias;
use Faker\Factory as Faker;


use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;

class CategoriasSeeder extends Seeder
{

    public function run()
    {
        $faker = Faker::create('pt_BR');

        Categorias::create([
            'titulo' => 'Garçom',
            'descricao' => 'Aquele que atende os clientes e leva os pratos até a mesa.',
            'icone' => "storage/categorias/categorias.png",
            'created_at' => now(),

        ]);

        Categorias::create([
            'titulo' => 'Barman',
            'descricao' => 'Aquele que serve coquetéis para os clientes.',
            'icone' => "storage/categorias/categorias.png",
            'created_at' => now(),

        ]);

        Categorias::create([
            'titulo' => 'Atendente',
            'descricao' => 'Aquele que é responsável pelo atendimento.',
            'icone' => "storage/categorias/categorias.png",
            'created_at' => now(),

        ]);

        Categorias::create([
            'titulo' => 'Cozinheiro',
            'descricao' => 'Aquele que cozinha a refeição.',
            'icone' => "storage/categorias/categorias.png",
            'created_at' => now(),

        ]);

        Categorias::create([
            'titulo' => 'Entregador',
            'descricao' => 'Aquele que realiza as entregas dos pedidos dos clientes.',
            'icone' => "storage/categorias/categorias.png",
            'created_at' => now(),

        ]);
    }
}
