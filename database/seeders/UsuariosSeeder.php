<?php

namespace Database\Seeders;

use App\Models\Categorias;
use App\Models\User;
use Faker\Factory as Faker;
use Illuminate\Support\Str;use Illuminate\Database\Seeder;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;

class UsuariosSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = Faker::create('pt_BR');
        $categoriaIds = Categorias::pluck('id');

        User::create([
            'nome' => 'administrador',
            'email' => 'adm@gmail.com',
            'cpf' => $faker->cpf(),
            'password' => bcrypt('12345678'),
            'data_nascimento' => $faker->date('Y-m-d'),
            'telefone' =>$faker->cellphoneNumber(),
            'descricao' =>$faker->text(100),
            'cep' => '175155555',
            'estado' => 'SP',
            'bairro' => 'Liberdade',
            'cidade' => 'São Paulo',
            'rua' => 'São Francisco',
            'numero' => 123,
            'nivel' => 'administrador',
            'categoria_id'=> 1,
            'created_at' => now(),
            'remember_token' => Str::random(2)


        ]);

        User::create([
            'nome' => 'administrador2',
            'email' => 'adm2@gmail.com',
            'cpf' => $faker->cpf(),
            'password' => bcrypt('12345678'),
            'data_nascimento' => $faker->date('Y-m-d'),
            'telefone' =>$faker->cellphoneNumber(),
            'descricao' =>$faker->text(100),
            'cep' => '175155555',
            'estado' => 'SP',
            'bairro' => 'Liberdade',
            'cidade' => 'São Paulo',
            'rua' => 'São Francisco',
            'numero' => 123,
            'nivel' => 'profissional',
            'categoria_id'=> 1,
            'created_at' => now(),
            'remember_token' => Str::random(2)


        ]);

        User::create([
            'nome' => 'administrador3',
            'email' => 'adm3@gmail.com',
            'cpf' => $faker->cpf(),
            'password' => bcrypt('12345678'),
            'data_nascimento' => $faker->date('Y-m-d'),
            'telefone' =>$faker->cellphoneNumber(),
            'descricao' =>$faker->text(100),
            'cep' => '175155555',
            'estado' => 'SP',
            'bairro' => 'Liberdade',
            'cidade' => 'São Paulo',
            'rua' => 'São Francisco',
            'numero' => 123,
            'nivel' => 'cliente',
            'categoria_id'=> 1,
            'created_at' => now(),
            'remember_token' => Str::random(2)


        ]);

        foreach (range(1, 20) as $index) {
            User::create([
            'nome' => $faker->name,
            'email' => $faker->unique()->safeEmail,
            'cpf' => $faker->cpf(),
            'password' => bcrypt('12345678'),
            'data_nascimento' => $faker->date('Y-m-d'),
            'telefone' =>$faker->cellphoneNumber(),
            'descricao' =>$faker->text(100),
            'cep' => '175155555',
            'estado' => 'SP',
            'bairro' => 'Liberdade',
            'cidade' => 'São Paulo',
            'rua' => 'São Francisco',
            'numero' => 123,
            'nivel' => 'Cliente',
            'categoria_id' => $categoriaIds->random(),
            'created_at' => now(),
            'remember_token' => Str::random(10)
            ]);
        }

    }
}
