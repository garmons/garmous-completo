<?php

namespace Database\Seeders;

use App\Models\Categorias;
use App\Models\Contratos;
use App\Models\User;
use Faker\Factory as Faker;


use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;

class ContratosSeeder extends Seeder
{

    public function run()
    {
        $faker = Faker::create('pt_BR');
        $fakerCategoria = Categorias::pluck('titulo');
        $categoriaIds = Categorias::pluck('id');
        $userIds = User::pluck('id');

        foreach(range(1, 10) as $index){
        Contratos::create([
            'titulo' => $fakerCategoria->random(),
            'descricao' => 'Aquele que atende os clientes e leva os pratos até a mesa.',
            'categoria_id' => $categoriaIds->random(),
            'user_id' => $userIds->random(),

            'created_at' => now(),

        ]);
    }
    }
}
