<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\HomeController;
use App\Http\Controllers\AdminController;
use App\Http\Controllers\LoginController;
use App\Http\Controllers\Admin\AnuncioController;
use App\Http\Controllers\Admin\ContratoController;
use App\Http\Controllers\Admin\UsuariosController;
use App\Http\Controllers\Admin\CategoriaController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', [HomeController::class, 'home'])->name('home');
Route::get('/sobre-nos', [HomeController::class, 'sobre'])->name('sobre-nos');
Route::get('/app', [HomeController::class, 'app'])->name('app');
Route::get('/ajuda', [HomeController::class, 'ajuda'])->name('ajuda');
Route::get('/login', [LoginController::class, 'login'])->name('login');
Route::post('/login', [LoginController::class, 'postLogin'])->name('post.login');
Route::get('/logout', [LoginController::class, 'logout'])->name('logout');
Route::get('/usuarios/cliente/pedido', [HomeController::class, 'pedido'])->name('cliente.pedido');


Route::get('/usuarios/cliente/cadastro', [HomeController::class, 'cadastroCliente'])->name('cliente.cadastro');
Route::get('/usuarios/profissional/cadastro', [HomeController::class, 'cadastroProfissional'])->name('cliente.Profissional');
Route::get('/usuarios/cliente', [HomeController::class, 'cliente'])->name('cliente');
Route::get('/usuarios/profissional', [HomeController::class, 'profissional'])->name('profissional');

// Route::get('/admin', [AdminController::class, 'home'])->name('admin.home');

Route::prefix('/admin')->middleware('auth')->group(function () {

    Route::get('/', [AdminController::class, 'home'])->name('admin.home');

    Route::get('/usuarios', [UsuariosController::class, 'index'])->name('admin.usuarios.index');
    Route::get('/usuarios/cadastrar', [UsuariosController::class, 'create'])->name('admin.usuarios.cadastrar');
    Route::post('/usuarios/cadastrar', [UsuariosController::class, 'store'])->name('admin.usuarios.cadastrar');
    Route::get('/usuarios/editar/{id}', [UsuariosController::class, 'edit'])->name('admin.usuarios.editar');
    Route::put('/usuarios/editar/{id}', [UsuariosController::class, 'update'])->name('admin.usuarios.editar');
    Route::delete('/usuarios/deletar/{id}', [UsuariosController::class, 'destroy'])->name('admin.usuarios.deletar');



    Route::get('/categorias', [CategoriaController::class, 'index'])->name('admin.categorias');
    Route::get('/categorias/cadastrar', [CategoriaController::class, 'create'])->name('admin.categorias.cadastrar');
    Route::post('/categorias/cadastrar', [CategoriaController::class, 'store'])->name('admin.categorias.cadastrar');
    Route::get('/categorias/editar/{id}', [CategoriaController::class, 'edit'])->name('admin.categorias.editar');
    Route::put('/categorias/editar/{id}', [CategoriaController::class, 'update'])->name('admin.categorias.editar');
    Route::delete('/categorias/deletar/{id}', [CategoriaController::class, 'destroy'])->name('admin.categorias.deletar');





    Route::get('/contratos', [ContratoController::class, 'index'])->name('admin.contratos.index');
    Route::get('/contratos/cadastrar', [ContratoController::class, 'create'])->name('admin.contratos.cadastrar');
    Route::post('/contratos/cadastrar', [ContratoController::class, 'store'])->name('admin.contratos.cadastrar');
    Route::get('/contratos/editar/{id}', [ContratoController::class, 'edit'])->name('admin.contratos.editar');
    Route::put('/contratos/editar/{id}', [ContratoController::class, 'update'])->name('admin.contratos.editar');
    Route::delete('/contratos/deletar/{id}', [ContratoController::class, 'destroy'])->name('admin.contratos.deletar');

});
