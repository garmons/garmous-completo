<?php

use App\Http\Controllers\Api\AuthController;
use App\Http\Controllers\Api\CategoriaController;
use App\Http\Controllers\Api\ContratoController;
use App\Http\Controllers\Api\UsuarioController;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Route;



Route::post('/login', [AuthController::class, 'login']);
Route::get('/logout', [AuthController::class, 'logout']);

Route::middleware('auth:sanctum')->group(function(){
    Route::get('/usuario', [UsuarioController::class, 'usuario']);
    Route::get('/logout', [AuthController::class, 'logout']);
});

Route::get('/categoria', [CategoriaController::class, 'listarCategorias']);
Route::get('/contrato', [ContratoController::class, 'listarContratos']);
Route::post('/usuarios/cadastrar-cliente', [UsuarioController::class, 'cadastrarCliente']);
Route::post('/usuarios/cadastrar-profissional', [UsuarioController::class, 'cadastrarProfissional']);
