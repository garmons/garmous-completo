<?php
require_once $_SERVER['DOCUMENT_ROOT'] . '/helpers/Config.php';
include_once CABECALHO;
?>

<!DOCTYPE html>
<html lang="pt-br">

    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.3.0-alpha1/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-GLhlTQ8iRABdZLl6O3oVMWSktQOp6b7In1Zl3/Jr59b6EGGoI1aFkw7cmDA6j6gD" crossorigin="anonymous">

    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/6.2.1/css/all.min.css" integrity="sha512-MV7K8+y+gLIBoVD59lQIYicR65iaqukzvf/nwasF0nqhPay5w/9lJmVM2hMDcnK1OnMGCdVK+iQrJ7lzPJQd1w==" crossorigin="anonymous" referrerpolicy="no-referrer" />

    <link href="/assets/css/mains.css" rel="stylesheet" type="text/css">

    <link rel="preconnect" href="https://fonts.googleapis.com">

    <link rel="preconnect" href="https://fonts.googleapis.com">
    <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
    <link href="https://fonts.googleapis.com/css2?family=Great+Vibes&family=Parisienne&display=swap" rel="stylesheet">

    <script src="/assets/js/main.js"></script>

    <div class=" fundoBackground  textoBranco">
        <div class="container">
            <div class=" row ">
                <div class="col-md-8 ">
                    <h1 class="fonteTitulo pt-5">Descubra os melhores profissionais!</h1>
                    <p>
                        Organize um evento contratando as melhores pessoas para trabalhar na sua festa,<br> de maneira simples e descomplicada.
                    </p>
                    <a href="#cadastro" class="btn btn-danger">Faça seu cadastro</a>
                </div>
                <div class="col-md-4">
                    <img id="imgBorda" src="/assets/img/trabalhadores.png" class="imgnone" width="400px">

                </div>
            </div>
        </div>
    </div>
    <h2 class="fonteTitulo py-5">Nossas categorias:</h2>
    <div class="container">
        <div class="row servico">
            <div class="col-md ">
                <a href="#">
                    <div class="fundoServico">
                        <h3>Garçom</h3>
                        <img src="/assets/img/ic_garcom.png" alt="garçom">
                    </div>
                </a>
            </div>
            <div class="col-md ">
                <a href="#">
                    <div class="fundoServico ">
                        <h3>Barman</h3>
                        <img src="/assets/img/ic_barman.png" alt="garçom">
                    </div>
                </a>
            </div>
            <div class="col-md ">
                <a href="#">
                    <div class="fundoServico ">
                        <h3>Atendente</h3>
                        <img src="/assets/img/ic_atendente.png" alt="garçom">
                    </div>
                </a>
            </div>
            <div class="col-md ">
                <a href="#">
                    <div class="fundoServico ">
                        <h3>Cozinheiro</h3>
                        <img src="/assets/img/ic_cozinheiro.png" alt="garçom">
                    </div>
                </a>
            </div>
            <div class="col-md ">
                <a href="#">
                    <div class="fundoServico ">
                        <h3>Entregador</h3>
                        <img src="/assets/img/ic_entregador.png" alt="garçom">
                    </div>
                </a>
            </div>
        </div>
    </div>

    <div class="fundoBackground2">
        <div class="container">
            <div class="row">
                <div class="col-md-6">
                    <img class="imgIlustrativa1 imgnone" src="/assets/img/pessoacelular.png" width="500px">
                </div>
                <div class="col-md-6">
                    <div class="textoBanner">
                        <h2 class="fonteSubTitulo">Encontre Profissionais!</h2>
                        <p>Em nosso site, temos varios profissionais cadastrados para facilitar o processo de contratação entre quem sabe fazer e quem busca o serviço.<br>
                            Tudo isso para facilitar a vida das pessoas que buscam organizar uma festa.
                        </p>
                    </div>
                </div>

            </div>
            <div class="row">
                <div class="col-md-6">

                    <div class="textoBanner">
                        <h2 class="fonteSubTitulo">Aumente seus clientes!</h2>
                        <p>Em nosso site, disponibilizaremos seu perfil para que varias pessoas descubram seu trabalho, aumentando sua visualização e o número de clientes
                        </p>
                    </div>
                </div>
                <div class="col-md-6">
                    <img class="imgIlustrativa2 imgnone" src="/assets/img/garçonete.png" width="500px">
                </div>
            </div>
        </div>
    </div>

    <section class="fundoBackground textoBranco">
        <div class="container ">
            <a name="cadastro"></a>
            <h2 class="fonteTitulo pt-4">Cadastre-se!</h2>

            <div class="row">
                <div class="col-md-6">
                    <div class="boxCadastre">

                        <h3>Quer novos clientes?</h3>
                        <p class="textCadastre">
                            Faça seu cadastro como profissional e encontre novos clientes, que precisam do seu serviço!
                        </p>

                        <div>
                            <a href="login2.html" class="btn btn-danger">Inscreva-se</a>
                        </div>
                    </div>
                </div>
                <div class="col-md-6 ">
                    <div class="boxCadastre">
                        <h3>Procura profissionais?</h3>
                        <p class="textCadastre">
                            Faça seu cadastro como cliente e contrate o profissional que mais se encaixa no seu pedido!
                        </p>

                        <div>
                            <a href="login1.html" class="btn btn-danger">Inscreva-se</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <div class="container">
        <h2 class="fonteTitulo py-3">Veja outras empresas:</h2>
        <p class="text-center">Desubra as empresas que tem parceria conosco e seu próprio negócio.</p>
        <div class="row">
            <div class="col-md-4 ">
                <a href="#">
                    <div class="fundoServico">
                        <h3>Empresa</h3>
                        <img src="" alt="">
                    </div>
                </a>
            </div>
            <div class="col-md-4 ">
                <a href="#">
                    <div class="fundoServico">
                        <h3>Empresa</h3>
                        <img src="" alt="">
                    </div>
                </a>
            </div>
            <div class="col-md-4 ">
                <a href="#">
                    <div class="fundoServico">
                        <h3>Empresa</h3>
                        <img src="" alt="">
                    </div>
                </a>
            </div>
        </div>
        <div class=" d-flex justify-content-center  py-4">
            <button type="button" class="btn btn-danger"><a>Veja Mais</a></button>
        </div>
    </div>
    <div class="card text-bg-dark">

    <img src="/assets/img/bannertwo.jpeg" class=" card-img" alt="banner mesa" height="350px">
        <div class="card-img-overlay">
            <h5 class="card-title fonteSubTitulo">"Somos a ponte entre o cliente e o profissional"</h5>
            <p class="card-text">Ajudando quem precisa de trabalho na área e quem quer contratar profissionais.</p>
            <p class="card-text"><small>Equipe Garmous</small></p>
        </div>
    </div>

<?php
include_once RODAPE;
?>

</html>