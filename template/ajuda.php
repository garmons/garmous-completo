<?php
require_once $_SERVER['DOCUMENT_ROOT'] . '/helpers/Config.php';
include_once CABECALHO;
?>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Garmous</title>

    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.3.0-alpha1/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-GLhlTQ8iRABdZLl6O3oVMWSktQOp6b7In1Zl3/Jr59b6EGGoI1aFkw7cmDA6j6gD" crossorigin="anonymous">

    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/6.2.1/css/all.min.css" integrity="sha512-MV7K8+y+gLIBoVD59lQIYicR65iaqukzvf/nwasF0nqhPay5w/9lJmVM2hMDcnK1OnMGCdVK+iQrJ7lzPJQd1w==" crossorigin="anonymous" referrerpolicy="no-referrer" />

    <link href="/assets/css/mains.css" rel="stylesheet" type="text/css">
    <link rel="preconnect" href="https://fonts.googleapis.com">

    <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
    <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
    <link href="https://fonts.googleapis.com/css2?family=Great+Vibes&family=Parisienne&display=swap" rel="stylesheet">

<body>


    <div class="fundoBackground">
        <h2 class="fonteTitulo textoBranco py-3">Passo a Passo</h2>
        <div class="container pb-3">
            <div class="row">
                <div class="col-md-4">
                    <div class="bordaPasso">
                        <h3>1. Faça seu Cadastro</h3>
                        <p>Registre-se em nosso site, e diga se você é um Profissional ou Cliente.
                        </p>
                        <i class="fa-regular fa-address-card tamanhoIcone"></i>
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="bordaPasso">
                        <h3>2. Descubra </h3>
                        <p>Enconte os melhores perfils de profissionais para contratar.
                        </p>
                        <i class="fa-solid fa-magnifying-glass tamanhoIcone"></i>
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="bordaPasso">
                        <h3>3. Processo Finalizado!</h3>
                        <p>Aceite a proposta, e realizem um contrato de trabalho.
                        </p>
                        <i class="fa-solid fa-circle-check tamanhoIcone"></i>
                    </div>
                </div>
            </div>
        </div>
    </div>


    <div class="container">
        <h2 class="fonteTitulo py-3">Perguntas Frequentes</h2>
        <div class="row">
            <div class="col-md-12">
                <div class="textoPerguntas">
                    <h3>O que preciso para me cadastrar?</h3>
                    <p>Para se cadastrar, clique no login e escolha entre opções de "Profissional" ou "Cliente", e
                        coloque os dados necessários. O cadastro só será concluido quando todas as informações
                        colocadas forem verificadas e validadas. Só aceitamos profissionais dos cargos descritos na página principal e no momento da inscrição.<br>
                        A área de login muda conforme o cadastro, mostraremos ao usuário "Profissional" apenas as pessoas que desejam contratar seus trabalhos conforme o cargo que foi escolhido.
                    </p>
                </div>
            </div>
            <div class="col-md-12">
                <div class="textoPerguntas">
                    <h3>Como funciona o processo de contatação?</h3>
                    <p>A Garmous funciona como o vínculo entre o cliente e o profissional, quando uma solicitação de
                        serviço for enviada por meio de um email, pode ser confirmada apenas pelo o usuário, porém,
                        isso não garante a contratação, recomendamos que entrem em contato e assinem um contrato de
                        serviço.
                    </p>
                </div>
            </div>
            <div class="col-md-12">
                <div class="textoPerguntas">
                    <h3>Onde meu perfil ficará disponível?</h3>
                    <p>O perfil de cada usuário cadastrado como "Profissional", ficará disponivel para os usuários
                        cadastrados como "Clientes" onde esse poderá solicitar seu serviço.
                        O perfil do usuário cadastrado como "Clientes" só ficará disponivel para visualização do
                        "Profissional", quando uma nova vaga for disponibilizada.<br>
                        Os perfils estarão disponiveis no site e no aplicativo.
                    </p>
                </div>
            </div>
            <div class="col-md-12">
                <div class="textoPerguntas">
                    <h3>Como disponibilizar um pedido de serviço?</h3>
                    <p>Para abrir uma vaga, é necessário o preenchimento do formulário, onde terá as informações
                        sobre a vaga (descrição, requisitos, pagamento, etc). Na área do usuário cadastrado como
                        "Cliente" terá a opção "Nova Vaga", toda vaga que for aberta, ficará disponível para o
                        "Profissional" e será disponibilizada apenas para o perfil do site.
                    </p>
                </div>
            </div>
        </div>
    </div>

    <form method="">
        <div class="fundoBackground2">

            <div class="container">
                <h2 class="fonteTitulo py-3">Duvidas? Fale Conosco!</h2>
                <div class="row">

                    <div class="col-md-8">
                        <label for="nomeDuvida" class="form-label">Nome:</label>
                        <input type="text" name="nomeDuvida" placeholder="Digite seu nome..." id="nomeDuvida" class="form-control"><br>


                        <label for="emailDuvida" class="form-label">Email:</label>
                        <input type="email" name="emailDvida" placeholder="Digite seu email..." id="nomeDuvida" class="form-control"><br>


                        <label for="pergunta">Pergunta:</label><br>
                        <textarea type="text" name="pergunta" placeholder="Digite sua pergunta..." id="pergunta" class="form-control"></textarea><br>


                        <div class="pb-3">
                            <button type="submit" class="btn btn-danger ">Enviar</button>
                        </div>
                    </div>

                    <div class="col-md-4">
                        <img id="imgBorda2" src="/assets/img/pessoaComputador.png" alt="duvida a pessoas" class="imgnone" width="300px">

                    </div>
                </div>
            </div>
        </div>

    </form>
</body>

<?php
include_once RODAPE;
?>

</html>