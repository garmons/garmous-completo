<?php
require_once "../helpers/Config.php";
include_once CABECALHO;
?>

<body>
    <div class="container">
        <div class="row">
            <div class="col-md-8">

                <form action="" method="post" class="row g-3">
                    <div class="col-md-6">
                        <label for="nome" class="form-label">Nome</label>
                        <input name="nome" type="text" id="nome" class="form-control">
                    </div>
                    <div class="col-md-3">
                        <label for="DataNascimento" class="form-label">Data nascimento</label>
                        <input name="DataNascimento" type="Datanascimento" id="DataNascimento" class="form-control">
                    </div>
                    <div class="col-md-6">
                        <label for="CPF" class="form-label">CPF</label>
                        <input name="CPF" type="CPF" id="CPF" class="form-control">
                    </div>
                    <div class="col-md-6">
                        <label for="email" class="form-label">E-mail</label>
                        <input name="email" type="email" id="email" class="form-control">
                    </div>
                    <div class="col-md-6">
                        <label for="Telefone" class="form-label">Telefone</label>
                        <input name="Telefone" type="Telefone" id="Telefone" class="form-control">
                    </div>

                    <div class="col-md-12 mt-3">
                        <h3>Endereço</h3>
                    </div>

                    <div class="col-md-3">
                        <label for="cep" class="form-label">CEP</label>
                        <input name="cep" type="text" id="cep" class="form-control">
                    </div>

                    <div class="col-md-7">
                        <label for="endereco" class="form-label">Endereço</label>
                        <input name="endereco" type="text" id="endereco" class="form-control">
                    </div>

                    <div class="col-md-2">
                        <label for="numero" class="form-label">Número</label>
                        <input name="numero" type="text" id="numero" class="form-control">
                    </div>

                    <div class="col-md-6">
                        <label for="complemento" class="form-label">Complemento</label>
                        <input name="complemento" type="text" id="complemento" class="form-control">
                    </div>
                    <div class="col-md-6">
                        <label for="bairro" class="form-label">Bairro</label>
                        <input name="bairro" type="text" id="bairro" class="form-control">
                    </div>
                    <div class="col-md-6">
                        <label for="cidade" class="form-label">Cidade</label>
                        <input name="cidade" type="text" id="cidade" class="form-control">
                    </div>

                    <div class="col-md-6">
                        <label for="estado" class="form-label">Estado</label>
                        <select name="estado" id="estado" class="form-control">
                            <option value="AC">Acre</option>
                            <option value="AL">Alagoas</option>
                            <option value="AP">Amapá</option>
                            <option value="AM">Amazonas</option>
                            <option value="BA">Bahia</option>
                            <option value="CE">Ceará</option>
                            <option value="DF">Distrito Federal</option>
                            <option value="ES">Espírito Santo</option>
                            <option value="GO">Goiás</option>
                            <option value="MA">Maranhão</option>
                            <option value="MT">Mato Grosso</option>
                            <option value="MS">Mato Grosso do Sul</option>
                            <option value="MG">Minas Gerais</option>
                            <option value="PA">Pará</option>
                            <option value="PB">Paraíba</option>
                            <option value="PR">Paraná</option>
                            <option value="PE">Pernambuco</option>
                            <option value="PI">Piauí</option>
                            <option value="RJ">Rio de Janeiro</option>
                            <option value="RN">Rio Grande do Norte</option>
                            <option value="RS">Rio Grande do Sul</option>
                            <option value="RO">Rondônia</option>
                            <option value="RR">Roraima</option>
                            <option value="SC">Santa Catarina</option>
                            <option value="SP">São Paulo</option>
                            <option value="SE">Sergipe</option>
                            <option value="TO">Tocantins</option>
                            <option value="EX">Estrangeiro</option>

                        </select>
                    </div>
                    <div class="col-md-4">
                        <label for="CNH" class="form-label">CNH</label>
                        <input name="CNH" type="text" id="CNH" class="form-control">
                    </div>
                    <div class="col-md-2">
                        <label for="Categoria" class="form-label">Categoria</label>
                        <input name="Categoria" type="text" id="Categoria" class="form-control">
                    </div>
                    <div class="col-md-6">
                        <label for="Cargo" class="form-label">Cargo</label>
                        <select name="Cargo" id="Cargo" class="form-control">
                            <option value="Nenhum">Nenhum</option>
                            <option value="Garcom">Garçom</option>
                            <option value="Atendente">Atendente</option>
                            <option value="Barmen">Barmen</option>
                            <option value="Cozinheiro">Cozinheiro</option>
                            <option value="Entregador">Entregador</option>
                        </select>
                    </div>
                    <div class="col-md-6">
                        <label for="Ensino" class="form-label">Ensino</label>
                        <select name="Ensino" id="Ensino" class="form-control">
                            <option value="Nenhum">Nenhum</option>
                            <option value="Fundamental">Fundamental</option>
                            <option value="FundamentalIncompleto">Fundamental Incompleto</option>
                            <option value="MedioCompleto">Medio Completo</option>
                            <option value="MedioIncompleto">Medio Incompleto</option>
                            <option value="SuperiorCompleto">Superior Completo</option>
                            <option value="SuperiorIncompleto">Superior Incompleto</option>
                        </select>
                    </div>

                    <div class="col-md-3">
                        <label for="Senha" class="form-label">Senha</label>
                        <input name="Senha" type="text" id="Senha" class="form-control">
                    </div>
                    <div class="col-md-3">
                        <label for="ConfirmaSenha" class="form-label">Confirma Senha</label>
                        <input name="ConfirmaSenha" type="text" id="ConfirmaSenha" class="form-control">
                    </div>

                    <div class="col-md-12 text-center" id="botaoEnviar">
                        <button class="btn btn-secondary">Enviar</button>
                    </div>


                </form>

            </div>
        </div>
    </div>
</body>

<?php
include_once RODAPE;
?>

</html>