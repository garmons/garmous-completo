<?php
require_once $_SERVER['DOCUMENT_ROOT'] . '/helpers/Config.php';
include_once CABECALHO;
?>

<!DOCTYPE html>
<html lang="pt-br">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Garmous</title>

    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.3.0-alpha1/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-GLhlTQ8iRABdZLl6O3oVMWSktQOp6b7In1Zl3/Jr59b6EGGoI1aFkw7cmDA6j6gD" crossorigin="anonymous">

    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/6.2.1/css/all.min.css" integrity="sha512-MV7K8+y+gLIBoVD59lQIYicR65iaqukzvf/nwasF0nqhPay5w/9lJmVM2hMDcnK1OnMGCdVK+iQrJ7lzPJQd1w==" crossorigin="anonymous" referrerpolicy="no-referrer" />

    <link href="/assets/css/mains.css" rel="stylesheet" type="text/css">
    <link rel="preconnect" href="https://fonts.googleapis.com">

    <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
    <link href="https://fonts.googleapis.com/css2?family=Great+Vibes&display=swap" rel="stylesheet">
    <script src="/assets/js/main.js"></script>


</head>

<body>
    <div class="fundoBackground">
        <div class="container">
            <div class="row">
                <div class="col-md-9">
                    <h2 class="fonteTitulo textoBranco">Baixe Nosso Aplicativo</h2>
                    <p class="textoBranco">
                        Veja as propostas de serviço, nossos profissionais, e facilite sua organização de<br> festas e muito mais no nosso app. <br> AppleStore & GooglePlay
                    </p>
                    <div class="py-3 d-flex justify-content-center">
                        <a class="btn btn-danger">Download</a>
                    </div>

                </div>

                <div class="col-md-3">
                    <img id="imgBorda" src="/assets/img/pessoacelular2.png" alt="Aplicativo Garmous" class="imgnone" width="300px">
                </div>

            </div>
        </div>
    </div>

    <div class="container">
        <h3 class="fonteTitulo py-5">Fácil, Rápido e Seguro</h3>
        <div class="row">
            <div class="col-md-6">
                <img id="imgBorda3" src="/assets/img/maos.png" alt="Foto Equipe" class="imgnone" width="400px">

            </div>
            <div class="col-md-6">
                <div id="menuFacilidades" class="pb-4">
                    <ul>
                        <li>Utilize o login de acesso</li>
                        <li>Personalize seu perfil</li>
                        <li>Tenha acesso as suas informações e configurações</li>
                        <li>Contrate os profissionais</li>
                        <li>Veja nossas dicas</li>
                        <li>Dúvidas frequentes</li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
</body>

<?php
include_once RODAPE;
?>

</html>