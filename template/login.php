<?php
require_once "../helpers/Config.php";
include_once CABECALHO;
?>

<div class="">
<link href="/assets/css/mains.css" rel="stylesheet" type="text/css">
    <div class="container">
     <div class="row">
        <div class="col-md-4" id="loginUsuario">
            <form>
                <!-- Email input -->
                <div class="form-outline mb-4">
                    <label class="form-label" for="email">E-mail</label>
                    <input type="email" id="email" class="form-control" />

                </div>

                <!-- Password input -->
                <div class="form-outline mb-4">
                    <label class="senha" for="form2Example2">senha</label>
                    <input type="password" id="senha" class="form-control" />

                </div>

                <!-- 2 column grid layout for inline styling -->
                <div class="row mb-4">
                    <div class="col d-flex justify-content-center">
                        <!-- Checkbox -->
                        <div class="form-check" id="checkUsuario">
                            <input class="form-check-input" type="checkbox" value="" id="checkboxLembrar" checked />
                            <label class="form-check-label" for="checkboxLembrar"> lembre-me </label>
                        </div>
                    </div>

                    <div class="col">
                        <!-- Simple link -->
                        <a href="#!">Não lembra a senha</a>
                    </div>
                </div>

                <!-- Submit button -->
                <div class="text-center">
                <button type="button" class="btn btn-primary btn-block mb-4">Login</button>

                <!-- Register buttons -->
                
                    <p>Não tem cadastro? <a href="index.html">cadastre-se</a></p>


                </div>
            </form>

        </div>
    </div>
</div>
   



<?php
include_once RODAPE;
?>