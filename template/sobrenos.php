 <?php
    require_once $_SERVER['DOCUMENT_ROOT'] . '/helpers/Config.php';
    include_once CABECALHO;

    ?>

 <!DOCTYPE html>
 <html lang="pt-br">

 <head>
     <meta charset="UTF-8">
     <meta http-equiv="X-UA-Compatible" content="IE=edge">
     <meta name="viewport" content="width=device-width, initial-scale=1.0">
     <title>Garmous</title>

     <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.3.0-alpha1/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-GLhlTQ8iRABdZLl6O3oVMWSktQOp6b7In1Zl3/Jr59b6EGGoI1aFkw7cmDA6j6gD" crossorigin="anonymous">

     <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/6.2.1/css/all.min.css" integrity="sha512-MV7K8+y+gLIBoVD59lQIYicR65iaqukzvf/nwasF0nqhPay5w/9lJmVM2hMDcnK1OnMGCdVK+iQrJ7lzPJQd1w==" crossorigin="anonymous" referrerpolicy="no-referrer" />

     <link href="/assets/css/mains.css" rel="stylesheet" type="text/css">
     <link rel="preconnect" href="https://fonts.googleapis.com">

     <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
     <link href="https://fonts.googleapis.com/css2?family=Great+Vibes&display=swap" rel="stylesheet">
     <script src="/assets/js/main.js"></script>


 </head>

 <body>
     <div class="fundoImagem">
         <div class="container ">
             <div class="row">

                 <div class="col-md textoBranco">
                     <h2 class="fonteTitulo ">Sobre Nós</h2>
                     <p> Somos uma empresa que conecta clientes e profissionais na área de eventos, trazendo beneficios a todos os usuários.<br> Nascemos em 2022 feito por um grupo de amigos, no curso "Tecnico em Informatica para Internet - Senac Marilia", com o propósito de ser a ponte entre quem precisa e quem sabe fazer, nosso desejo é fornecer aos nossos clientes e profissionais a confiabilidade entre eles.</p>
                 </div>

                 <div class="col-md ">
                     <img id="imgBorda2" src="/assets/img/senacmarilia.jpg" alt="Senac Marilia-SP" class="imgnone " width="500px">
                 </div>
             </div>
         </div>
     </div>


     <div class="container ">
         <div class="row">
             <div class="col-md-4 ">
                 <div class="missaoVisaoValores ">
                     <i class="fa-solid fa-rocket tamanhoIcone"></i>
                     <h3>Missão</h3>
                     <p>
                         Conectar os cliente e trabalhadores para facilitar a organização de festas e eventos.
                     </p>
                 </div>
             </div>
             <div class="col-md-4 ">
                 <div class="missaoVisaoValores">
                     <i class="fa-sharp fa-solid fa-eye tamanhoIcone"></i>
                     <h3>Visão</h3>
                     <p>
                         Expandir nossa empresa de forma que venha beneficiar os trabalhadores com mais oportunidades, e os clientes com profissionais capacitados.
                     </p>
                 </div>
             </div>
             <div class="col-md-4">
                 <div class="missaoVisaoValores">
                     <i class="fa-solid fa-handshake tamanhoIcone"></i>
                     <h3>Valores</h3>
                     <p>
                         Ética Profissional e segurança e confiabilidade em nossos serviços.
                     </p>
                 </div>
             </div>
         </div>
     </div>

 </body>

 <?php
    include_once RODAPE;
    ?>

 </html>