<?php

namespace App\Http\Controllers\Admin;

use Exception;
use App\Models\Contrato;
use App\Models\Contratos;
use App\Models\Categorias;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class ContratoController extends Controller
{

    public function index()
    {
        $contratos = Contratos::all();
        return view('admin.contratos.index', [
            'contratos' => $contratos,
        ]);
    }

    public function create()
    {
        return view('admin.contratos.cadastrar', [
            'contratos' => new Contratos(),
            'categorias' => Categorias::all(),
        ]);
    }


    public function store(Request $request)
    {
         //dd($request);
         $request->validate([
            'titulo' => 'required',
            'descricao' => 'required',
            'categoria_id' => 'required',

         ]);

        try {

            $contratos = new Contratos();
            $contratos->titulo = $request->titulo;
            $contratos->descricao = $request->descricao;
            $contratos->categoria_id = $request->categoria_id;
            $contratos->save();

            return redirect()->route('admin.contratos.index')->with('sucesso', 'Contrato cadastrado com sucesso!');
        } catch (\Exception $e) {

            //dd($e->getMessage());

            return redirect()->back()->withInput()->with('erro', 'Ocorreu um erro ao cadastrar, por favor tente novamente');
        }
    }




    public function edit($id)
    {
        return view('admin.contratos.editar', [
            'contratos' => Contratos::findOrFail($id),
            'categorias' => Categorias::all(),
        ]);
    }


    public function update(Request $request, $id)
    {
         //dd($request);
         $request->validate([
            'titulo' => 'required',
            'descricao' => 'required',
            'categoria_id' => 'required',

         ]);

        try {

            $contratos = Contratos::findOrFail($id);
            $contratos->titulo = $request->titulo;
            $contratos->descricao = $request->descricao;
            $contratos->categoria_id = $request->categoria_id;

            $contratos->save();

            return redirect()->route('admin.contratos.index')->with('sucesso', 'Contrato editado com sucesso!');
        } catch (\Exception $e) {

            //dd($e->getMessage());

            return redirect()->back()->withInput()->with('erro', 'Ocorreu um erro ao editar , por favor tente novamente');
        }
    }


    public function destroy($id)
    {
        try {
            Contratos::findOrFail($id)->delete();

            return redirect()->route('admin.contratos.index')->with('sucesso', 'Contrato deletado com sucesso');
        } catch (Exception $e) {

            return redirect()->back()->with('erro', 'Ocorreu um erro ao deletar, por favor tente novamente');
        }
    }
}
