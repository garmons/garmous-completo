<?php

namespace App\Http\Controllers\Admin;

use Exception;
use App\Models\Categorias;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Storage;

class CategoriaController extends Controller
{

    public function index()
    {

        $categorias = Categorias::all();
        return view('admin.categorias.index',  [
            'categorias' => $categorias,
        ]);
    }



    public function create()
    {
        return view('admin.categorias.cadastrar', [
            'categorias' => new Categorias()
        ]);
    }


    public function store(Request $request, )
    {

        //dd($request);
        $request->validate([
            'titulo' => 'required',
            'descricao' => 'required',
            'icone' => 'required',

        ]);

        try {

            $categorias = new Categorias();
            $categorias->titulo = $request->titulo;
            $categorias->descricao = $request->descricao;

            if($request->hasFile('icone')){

                //Upload com nome do random do arquivo..
                $iconePath = $request->file('icone')->store('public/categorias');
                $categorias->icone = Storage::url($iconePath);
            }
            $categorias->save();


            return redirect()->route('admin.categorias')->with('sucesso', 'Categoria cadastrada com sucesso!');
        } catch (\Exception $e) {

            //dd($e->getMessage());

            return redirect()->back()->withInput()->with('erro', 'Ocorreu um erro ao cadastrar, por favor tente novamente');
        }
    }

    public function edit($id)
    {
        return view('admin.categorias.editar', [
            'categorias' => Categorias::findOrFail($id)
        ]);
    }


    public function update(Request $request, $id)
    {
        $request->validate([
            'titulo' => 'required',
            'descricao' => 'required',
            'icone' => 'required',
        ]);

        try {

            $categorias = Categorias::findOrFail($id);
            $categorias->titulo = $request->titulo;
            $categorias->descricao = $request->descricao;

            if($request->hasFile('icone')){
                Storage::delete('public/categorias/'.basename($categorias->icone));

                  //Upload com nome do random do arquivo..
                  $iconePath = $request->file('icone')->store('public/categorias');
                  $categorias->icone = Storage::url($iconePath);
            }

            $categorias->save();

            return redirect()->route('admin.categorias')->with('sucesso', 'Categoria editada com sucesso');
        } catch (\Exception $e) {

            //dd($e->getMessage());

            return redirect()->back()->withInput()->with('erro', 'Ocorreu um erro ao editar, por favor tente novamente');
        }
    }


    public function destroy($id)
    {
        try {
            Categorias::findOrFail($id)->delete();

            return redirect()->route('admin.categorias')->with('sucesso', 'Categoria deletada com sucesso');
        } catch (Exception $e) {

            return redirect()->back()->with('erro', 'Ocorreu um erro ao deletar, por favor tente novamente');
        }
    }
}
