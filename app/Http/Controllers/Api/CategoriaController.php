<?php

namespace App\Http\Controllers\Api;

use App\Models\Categoria;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Categorias;

class CategoriaController extends Controller
{
    public function listarCategorias()
    {
        $categorias = Categorias::all(['id', 'titulo', 'descricao', 'icone']);

        $dadosCategorias = $categorias->map(function ($categoria) {
            $categoria->titulo = $categoria->titulo;
            // $categoria->descricao = $categoria->descricao;
            $categoria->icone = url($categoria->icone);
            return $categoria;
        });

        return response()->json($dadosCategorias);
    }
}
