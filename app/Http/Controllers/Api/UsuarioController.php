<?php

namespace App\Http\Controllers\Api;

use App\Models\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Validator;

class UsuarioController extends Controller
{
    public function usuario(Request $request)
    {
        $usuario = $request->user();

        $dadosUsuario = [
            'nome' => $usuario->nome,
            'email' => $usuario->email,
            'foto' => url($usuario->foto),
            'cpf' => $usuario->cpf,
            'data_nascimento' => $usuario->data_nascimento,
            'telefone' => $usuario->telefone,
            'descricao' => $usuario->descricao,
            'cep' => $usuario->cep,
            'estado' => $usuario->estado,
            'bairro' => $usuario->bairro,
            'cidade' => $usuario->cidade,
            'rua' => $usuario->rua,
            'numero' => $usuario->numero,
            'categoria_id' => $usuario->categoria_id,
            'nivel' => $usuario->nivel,

        ];

        return response()->json($dadosUsuario);
    }

    public function cadastrarCliente(Request $request)
    {

        // Validação dos dados de entrada
        $validator = Validator::make($request->all(), [
            'nome' => 'required',
            'email' => 'required|email|unique:users',
            'password' => 'required',
            'cpf' => 'required',
            'data_nascimento' => 'required',
            'telefone' => 'required',
            // 'descricao' => 'required',
            // 'cep' => 'required',
            // 'estado' => 'required',
            // 'bairro' => 'required',
            // 'cidade' => 'required',
            // 'rua' => 'required',
            // 'numero' => 'required',
            'categoria_id' => 'required',
            // // 'nivel' => 'required',
        ]);

        // Verifica se houve falha na validação
        if ($validator->fails()) {
            $errors = $validator->errors()->all();
            return response()->json(['errors' => $errors], 422);
        }

        // Salvando o usuário e tratando possíveis exceções
        try {
            $usuario = new User();
            $usuario->nome = $request->nome;
            $usuario->email = $request->email;
            $usuario->password = bcrypt($request->password);
            $usuario->cpf = $request->cpf ;
            $usuario->data_nascimento = $request->data_nascimento;
            $usuario->telefone = $request->telefone;
            // $usuario->descricao = $request->descricao;
            // $usuario->cep = $request->cep;
            // $usuario->estado = $request->estado;
            // $usuario->bairro = $request->bairro;
            // $usuario->cidade = $request->cidade;
            // $usuario->rua = $request->rua;
            // $usuario->numero = $request->numero;
            $usuario->categoria_id = $request->categoria_id;
            // $usuario->nivel = $request->nivel;



            if ($request->hasFile('foto')) {
                $usuarioPath = $request->file('foto')->store('public/usuarios');
                $usuario->foto = Storage::url($usuarioPath);
            }

            $usuario->save();

            return response()->json($usuario);
        } catch (\Exception $e) {
            return response()->json(['error' => "Ocorreu um erro ao cadastrar o Usuario.$e"], 500);
        }
    }









    public function cadastrarProfissional(Request $request)
    {

        // Validação dos dados de entrada
        $validator = Validator::make($request->all(), [
            'nome' => 'required',
            'email' => 'required|email|unique:users',
            'password' => 'required',
            'cpf' => 'required',
            'data_nascimento' => 'required',
            'telefone' => 'required',
            'descricao' => 'required',
            'cep' => 'required',
            'estado' => 'required',
            'bairro' => 'required',
            'cidade' => 'required',
            'rua' => 'required',
            'numero' => 'required',
            'categoria_id' => 'required',
            'nivel' => 'required',

        ]);

        // Verifica se houve falha na validação
        if ($validator->fails()) {
            $errors = $validator->errors()->all();
            return response()->json(['errors' => $errors], 422);
        }

        // Salvando o usuário e tratando possíveis exceções
        try {
            $usuario = new User();
            $usuario->nome = $request->nome;
            $usuario->email = $request->email;
            $usuario->password = bcrypt($request->password);
            $usuario->cpf = $request->cpf ;
            $usuario->data_nascimento = $request->data_nascimento;
            $usuario->telefone = $request->telefone;
            $usuario->descricao = $request->descricao;
            $usuario->cep = $request->cep;
            $usuario->estado = $request->estado;
            $usuario->bairro = $request->bairro;
            $usuario->cidade = $request->cidade;
            $usuario->rua = $request->rua;
            $usuario->numero = $request->numero;
            $usuario->categoria_id = $request->categoria_id;
            $usuario->nivel = $request->nivel;

            if ($request->hasFile('foto')) {
                $usuarioPath = $request->file('foto')->store('public/usuarios');
                $usuario->foto = Storage::url($usuarioPath);
            }

            $usuario->save();

            return response()->json($usuario);
        } catch (\Exception $e) {
            return response()->json(['error' => "Ocorreu um erro ao cadastrar o Usuario."], 500);
        }
    }
}
