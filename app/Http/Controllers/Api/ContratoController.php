<?php

namespace App\Http\Controllers\Api;

use App\Models\Categoria;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Categorias;
use App\Models\Contratos;

class ContratoController extends Controller
{
    public function listarContratos()
    {
        $contratos = Contratos::all(['user_id', 'categoria_id', 'titulo', 'descricao']);

        $dadosContratos = $contratos->map(function ($contrato) {
            $contrato->user_id = $contrato->user_id;
            $contrato->categoria_id = $contrato->categoria_id;
            $contrato->titulo = $contrato->titulo;
            $contrato->descricao = $contrato->descricao;
            return $contrato;
        });

        return response()->json($dadosContratos);
    }
}
