<?php

namespace App\Http\Controllers;

use App\Models\Categorias;
use Illuminate\Http\Request;

class HomeController extends Controller
{
    public function home()
    {
        return view('site.home',[
            'categorias' => Categorias::all()
        ]);

    }

    public function sobre()
    {
        return view('site.sobre');
    }

    public function app()
    {
        return view('site.app');
    }

    public function ajuda()
    {
        return view('site.ajuda');
    }

    public function cliente()
    {
        return view('usuarios.cliente');
    }
    public function profissional()
    {
        return view('usuarios.profissional');
    }
    public function cadastroCliente(){
        return view('cadastro.cadastro-cliente', [

        ]);

    }
    public function cadastroProfissional(){
        return view("cadastro.cadastro-profissional",[
        'categorias' => Categorias::all()
        ]);
    }

    public function pedido(){
        return view('usuarios.pedido',[
            'categorias' => Categorias::all()
            ]);
    }



}
