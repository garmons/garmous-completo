@extends('layouts.site')

@section('titulo', 'Sobre Nós')

@section('conteudo')


    <div class="fundoImagem">
        <div class="container ">
            <div class="row">

                <div class="col-md textoBranco">
                    <h2 class="fonteTitulo ">Sobre Nós</h2>
                    <p> Somos uma empresa que conecta clientes e profissionais na área de eventos, trazendo beneficios a
                        todos os usuários.<br> Nascemos em 2022 feito por um grupo de amigos, no curso "Tecnico em
                        Informatica para Internet - Senac Marilia", com o propósito de ser a ponte entre quem precisa e quem
                        sabe fazer, nosso desejo é fornecer aos nossos clientes e profissionais a confiabilidade entre eles.
                    </p>
                </div>

                <div class="col-md ">
                    <img id="imgBorda2" src="/assets/img/senacmarilia.jpg" alt="Senac Marilia-SP" class="imgnone "
                        width="500px">
                </div>
            </div>
        </div>
    </div>


    <div class="container ">
        <div class="row">
            <div class="col-md-4 ">
                <div class="missaoVisaoValores ">
                    <i class="fa-solid fa-rocket tamanhoIcone"></i>
                    <h3>Missão</h3>
                    <p>
                        Conectar os cliente e trabalhadores para facilitar a organização de festas e eventos.
                    </p>
                </div>
            </div>
            <div class="col-md-4 ">
                <div class="missaoVisaoValores">
                    <i class="fa-sharp fa-solid fa-eye tamanhoIcone"></i>
                    <h3>Visão</h3>
                    <p>
                        Expandir nossa empresa de forma que venha beneficiar os trabalhadores com mais oportunidades, e os
                        clientes com profissionais capacitados.
                    </p>
                </div>
            </div>
            <div class="col-md-4">
                <div class="missaoVisaoValores">
                    <i class="fa-solid fa-handshake tamanhoIcone"></i>
                    <h3>Valores</h3>
                    <p>
                        Ética Profissional e segurança e confiabilidade em nossos serviços.
                    </p>
                </div>
            </div>
        </div>
    </div>
@endsection
