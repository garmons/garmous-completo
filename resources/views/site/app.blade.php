@extends('layouts.site')

@section('titulo', 'App')

@section('conteudo')

    <div class="fundoBackground">
        <div class="container">
            <div class="row">
                <div class="col-md-9">
                    <h2 class="fonteTitulo textoBranco">Baixe Nosso Aplicativo</h2>
                    <p class="textoBranco">
                        Veja as propostas de serviço, nossos profissionais, e facilite sua organização de<br> festas e muito
                        mais no nosso app. <br> AppleStore & GooglePlay
                    </p>
                    <div class="py-3 d-flex justify-content-center">
                        <a class="btn btn-danger">Download</a>
                    </div>

                </div>

                <div class="col-md-3">
                    <img id="imgBorda" src="/assets/img/pessoacelular2.png" alt="Aplicativo Garmous" class="imgnone"
                        width="300px">
                </div>

            </div>
        </div>
    </div>

    <div class="container">
        <h3 class="fonteTitulo py-5">Fácil, Rápido e Seguro</h3>
        <div class="row">
            <div class="col-md-6">
                <img id="imgBorda3" src="/assets/img/maos.png" alt="Foto Equipe" class="imgnone" width="400px">

            </div>
            <div class="col-md-6">
                <div id="menuFacilidades" class="pb-4">
                    <ul>
                        <li>Utilize o login de acesso</li>
                        <li>Personalize seu perfil</li>
                        <li>Tenha acesso as suas informações e configurações</li>
                        <li>Contrate os profissionais</li>
                        <li>Veja nossas dicas</li>
                        <li>Dúvidas frequentes</li>
                    </ul>
                </div>
            </div>
        </div>
    </div>



@endsection
