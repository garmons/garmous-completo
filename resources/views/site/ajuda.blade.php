@extends('layouts.site')

@section('titulo', 'Ajudo')

@section('conteudo')

    <div class="fundoBackground">
        <h2 class="fonteTitulo textoBranco py-3">Passo a Passo</h2>
        <div class="container pb-3">
            <div class="row">
                <div class="col-md-4">
                    <div class="bordaPasso">
                        <h3>1. Faça seu Cadastro</h3>
                        <p>Registre-se em nosso site, e diga se você é um Profissional ou Cliente.
                        </p>
                        <i class="fa-regular fa-address-card tamanhoIcone"></i>
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="bordaPasso">
                        <h3>2. Descubra </h3>
                        <p>Enconte os melhores perfils de profissionais para contratar.
                        </p>
                        <i class="fa-solid fa-magnifying-glass tamanhoIcone"></i>
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="bordaPasso">
                        <h3>3. Processo Finalizado!</h3>
                        <p>Aceite a proposta, e realizem um contrato de trabalho.
                        </p>
                        <i class="fa-solid fa-circle-check tamanhoIcone"></i>
                    </div>
                </div>
            </div>
        </div>
    </div>


    <div class="container">
        <h2 class="fonteTitulo py-3">Perguntas Frequentes</h2>
        <div class="row">
            <div class="col-md-12">
                <div class="textoPerguntas">
                    <h3>O que preciso para me cadastrar?</h3>
                    <p>Para se cadastrar, clique no login e escolha entre opções de "Profissional" ou "Cliente", e
                        coloque os dados necessários. O cadastro só será concluido quando todas as informações
                        colocadas forem verificadas e validadas. Só aceitamos profissionais dos cargos descritos na página
                        principal e no momento da inscrição.<br>
                        A área de login muda conforme o cadastro, mostraremos ao usuário "Profissional" apenas as pessoas
                        que desejam contratar seus trabalhos conforme o cargo que foi escolhido.
                    </p>
                </div>
            </div>
            <div class="col-md-12">
                <div class="textoPerguntas">
                    <h3>Como funciona o processo de contatação?</h3>
                    <p>A Garmous funciona como o vínculo entre o cliente e o profissional, quando uma solicitação de
                        serviço for enviada por meio de um email, pode ser confirmada apenas pelo o usuário, porém,
                        isso não garante a contratação, recomendamos que entrem em contato e assinem um contrato de
                        serviço.
                    </p>
                </div>
            </div>
            <div class="col-md-12">
                <div class="textoPerguntas">
                    <h3>Onde meu perfil ficará disponível?</h3>
                    <p>O perfil de cada usuário cadastrado como "Profissional", ficará disponivel para os usuários
                        cadastrados como "Clientes" onde esse poderá solicitar seu serviço.
                        O perfil do usuário cadastrado como "Clientes" só ficará disponivel para visualização do
                        "Profissional", quando uma nova vaga for disponibilizada.<br>
                        Os perfils estarão disponiveis no site e no aplicativo.
                    </p>
                </div>
            </div>
            <div class="col-md-12">
                <div class="textoPerguntas">
                    <h3>Como disponibilizar um pedido de serviço?</h3>
                    <p>Para abrir uma vaga, é necessário o preenchimento do formulário, onde terá as informações
                        sobre a vaga (descrição, requisitos, pagamento, etc). Na área do usuário cadastrado como
                        "Cliente" terá a opção "Nova Vaga", toda vaga que for aberta, ficará disponível para o
                        "Profissional" e será disponibilizada apenas para o perfil do site.
                    </p>
                </div>
            </div>
        </div>
    </div>

    <form method="">
        <div class="fundoBackground2">

            <div class="container">
                <h2 class="fonteTitulo py-3">Duvidas? Fale Conosco!</h2>
                <div class="row">

                    <div class="col-md-8">
                        <label for="nomeDuvida" class="form-label">Nome:</label>
                        <input type="text" name="nomeDuvida" placeholder="Digite seu nome..." id="nomeDuvida"
                            class="form-control"><br>


                        <label for="emailDuvida" class="form-label">Email:</label>
                        <input type="email" name="emailDvida" placeholder="Digite seu email..." id="nomeDuvida"
                            class="form-control"><br>


                        <label for="pergunta">Pergunta:</label><br>
                        <textarea type="text" name="pergunta" placeholder="Digite sua pergunta..." id="pergunta" class="form-control"></textarea><br>


                        <div class="pb-3">
                            <button type="submit" class="btn btn-danger ">Enviar</button>
                        </div>
                    </div>

                    <div class="col-md-4">
                        <img id="imgBorda2" src="/assets/img/pessoaComputador.png" alt="duvida a pessoas" class="imgnone"
                            width="300px">

                    </div>
                </div>
            </div>
        </div>

    </form>

@endsection
