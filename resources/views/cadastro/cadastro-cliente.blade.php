@extends('layouts.site')

@section('titulo', 'Bem Vindo')

@section('conteudo')

    <body>
        <div class="container LoginCadastro">
            <div class="row">
                <div class="col-md-8 p-5">


                    <form action="" method="post" class="row g-3">
                        <div class="col-md-6">
                            <label for="nome" class="form-label">Nome</label>
                            <input name="nome" type="text" id="nome" class="form-control">
                        </div>
                        <div class="col-md-3">
                            <label for="DataNascimento" class="form-label">Data nascimento</label>
                            <input name="DataNascimento" type="Datanascimento" id="DataNascimento" class="form-control">
                        </div>
                        <div class="col-md-6">
                            <label for="CPF" class="form-label">CPF</label>
                            <input name="CPF" type="CPF" id="CPF" class="form-control">
                        </div>
                        <div class="col-md-6">
                            <label for="email" class="form-label">E-mail</label>
                            <input name="email" type="email" id="email" class="form-control">
                        </div>

                        <div class="col-md-12">
                            <label for="Telefone" class="form-label">Telefone</label>
                            <input name="Telefone" type="Telefone" id="Telefone" class="form-control">
                        </div>

                        <div class="col-md-3">
                            <label for="Senha" class="form-label">Senha</label>
                            <input name="Senha" type="text" id="Senha" class="form-control">
                        </div>
                        <div class="col-md-3">
                            <label for="ConfirmaSenha" class="form-label">Confirma Senha</label>
                            <input name="ConfirmaSenha" type="text" id="ConfirmaSenha" class="form-control">
                        </div>

                        <div class="col-md-12 text-center" id="botaoEnviar">
                            <button class="btn btn-danger">Enviar</button>
                        </div>


                    </form>

                </div>
            </div>
        </div>
    </body>



@endsection
