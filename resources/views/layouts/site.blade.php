<!DOCTYPE html>
<html lang="pt-br">

<link href="https://cdn.jsdelivr.net/npm/bootstrap@5.3.0-alpha1/dist/css/bootstrap.min.css" rel="stylesheet"
    integrity="sha384-GLhlTQ8iRABdZLl6O3oVMWSktQOp6b7In1Zl3/Jr59b6EGGoI1aFkw7cmDA6j6gD" crossorigin="anonymous">

<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/6.2.1/css/all.min.css"
    integrity="sha512-MV7K8+y+gLIBoVD59lQIYicR65iaqukzvf/nwasF0nqhPay5w/9lJmVM2hMDcnK1OnMGCdVK+iQrJ7lzPJQd1w=="
    crossorigin="anonymous" referrerpolicy="no-referrer" />

<link href="/assets/css/mains.css" rel="stylesheet" type="text/css">

<link rel="preconnect" href="https://fonts.googleapis.com">

<link rel="preconnect" href="https://fonts.googleapis.com">
<link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
<link href="https://fonts.googleapis.com/css2?family=Great+Vibes&family=Parisienne&display=swap" rel="stylesheet">


<script type="text/javascript" src="/assets/js/main.js"></script>

</head>

<header>
    <div class="fundoBackground">
        <div class="container">
            <div class="row">

                <div id="logotipo" class="col-md-2 col-10">
                    <a href="{{ route('home') }}"><img src="/assets/img/logogarmous.png" alt="logo Garmous"
                            width="100"></a>
                </div>

                <div class="col-2 d-block d-lg-none">
                    <button id="btnMenu" class="btn mt-3">
                        <i class="fas fa-bars"></i>
                    </button>
                </div>

                <nav id="menu" class="col-md-10  ">
                    <div class="d-flex justify-content-end ">
                        <ul>
                            <li><a href="{{ route('sobre-nos') }}" class="botaoFundo">Sobre Nós</a></li>
                            <li><a href="{{ route('ajuda') }}" class="botaoFundo">Ajuda</a></li>
                            <li><a href="{{ route('app') }}" class="botaoFundo">App</a></li>
                            <li><a href="{{ route('login') }}" class="btn btn-danger" >Login</a></li>
                        </ul>
                    </div>

                </nav>
            </div>

        </div>
</header>

<main>

    @yield('conteudo')
</main>

<footer class="textoBranco fundoBackground py-3">
    <div class="container">
        <div class="row">
            <div class="col-md-3">

                <h4>Institucional</h4>
                <li><a href="{{ route('home') }}" class="textoBranco">Home</a></li>
                <li><a href="{{ route('sobre-nos') }}" class="textoBranco">Sobre Nós</a></li>
                <li><a href="{{ route('ajuda') }}" class="textoBranco">Ajuda</a></li>
                <li><a href="{{ route('app') }}" class="textoBranco">App</a></li>


            </div>
            <div class="col-md-3">

                <h4>Categorias</h4>

                <li><a href="garcom.html" class="textoBranco">Garçom</a></li>
                <li><a href="entregador.html" class="textoBranco">Entregador</a></li>
                <li><a href="atendente.html" class="textoBranco">Atendente</a></li>
                <li><a href="barman.html" class="textoBranco">Barman</a></li>
                <li><a href="cozinheiro.html" class="textoBranco">Cozinheiro</a></li>

            </div>

            <div class="col-md-3">

                <h4>Redes Sociais</h4>

                <a href="#"><i class="fa-brands fa-facebook-f textoBranco"></i></a>
                <a href="#"><i class="fa-brands fa-instagram textoBranco"></i></a>
                <a href="#"><i class="fa-brands fa-linkedin textoBranco"></i></a>

            </div>

            <div class="col-md-3">

                <h4>Downloads</h4>
                <a href="#"><img src="/assets/img/googleplay.png" class="img-fluid" alt="Googleplay"
                        width="200px"></a>
                <a href="#"><img src="/assets/img/aplestore.png" class="img-fluid" alt="AppleStore"
                        width="200px"></a>
            </div>
        </div>
    </div>
</footer>


<div id="copyright">
    <p>
        Termos de Uso - Copyright - Politica de Privacidade
    </p>
</div>
